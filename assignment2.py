from flask import Flask, render_template, request, redirect, url_for, jsonify
import random

app = Flask(__name__)


# -------- GLOBAL "DATABASE" AND FUNCTIONS --------
books = [{'title': 'Software Engineering', 'id': '1'}, {'title': 'Algorithm Design', 'id':'2'}, {'title': 'Python', 'id':'3'}]

def find_index(book_id):
    for i in books:
        if i['id'] == str(book_id):
            return books.index(i)

def find_max_id():
    ids = []
    for i in books:
        ids.append(int(i['id']))
    return max(ids)


# -------- JSON PAGE --------
@app.route('/book/JSON')
def bookJSON():
    r = json.dumps(books)
    return Response(r)


# -------- SHOW BOOK --------
@app.route('/')
@app.route('/book/')
def showBook():
    return render_template('showBook.html', books=books)


# -------- NEW BOOK --------
@app.route('/book/new/', methods=['GET', 'POST'])
def newBook():
    if request.method == 'POST':
        new_title = request.form['name']
        new_id = str(find_max_id() + 1)
        new_entry = {'title':new_title, 'id':new_id}
        books.append(new_entry)
        return redirect(url_for('showBook'))
    else:
        return render_template('newBook.html')


# -------- EDIT BOOK --------
@app.route('/book/<int:book_id>/edit/', methods=['GET','POST'])
def editBook(book_id):
    if request.method == 'POST':
        idx = find_index(book_id)
        books[idx]['title'] = request.form['name']
        return redirect(url_for('showBook'))
    else:
        return render_template('editBook.html', book_id=book_id)


# -------- DELETE BOOK --------
@app.route('/book/<int:book_id>/delete/', methods = ['GET', 'POST'])
def deleteBook(book_id):
    if request.method == 'POST':
        idx = find_index(book_id)
        books.pop(idx)
        return redirect(url_for('showBook'))
    else:
        return render_template('deleteBook.html', book_id=book_id)


# -------- MAIN --------
if __name__ == '__main__':
	app.debug = True
	app.run(host='127.0.0.1', port=5000)
	

